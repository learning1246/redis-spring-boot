package com.jt.redis.redis_as_db.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@RedisHash("expenses")
public class ExpenseItem implements Serializable {

    @Id
    private String id;
    private String descrption;
    private Date date;
    private BigDecimal price;

}
