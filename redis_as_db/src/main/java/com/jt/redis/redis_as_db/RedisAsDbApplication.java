package com.jt.redis.redis_as_db;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jt.redis.redis_as_db.entity.ExpenseItem;
import com.jt.redis.redis_as_db.repository.ExpenseRepository;

@SpringBootApplication
@RestController
@RequestMapping("/expenses")
@EnableCaching
public class RedisAsDbApplication {

	@Autowired
	ExpenseRepository repository;

	public static void main(String[] args) {
		SpringApplication.run(RedisAsDbApplication.class, args);
	}

	@PostMapping
	@CachePut(key = "#result.id", value = "expenses")
	public ExpenseItem save(@RequestBody ExpenseItem expenseItem) {
		expenseItem.setId(UUID.randomUUID().toString());
		return repository.save(expenseItem);
	}

	@GetMapping("/all")
	public List<Object> findAll() {
		return repository.findAll();
	}

	@GetMapping("/{id}")
	@Cacheable(key = "#id", value = "expenses", unless = "#result == null")
	public Object findById(@PathVariable String id) {
		return repository.findById(id);
	}

	@DeleteMapping("/{id}")
	@CacheEvict(key = "#id", value = "expenses")
	public void deleteById(@PathVariable String id) {
		repository.deleteById(id);
	}

}
