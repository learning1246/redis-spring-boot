package com.jt.redis.redis_as_db.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import com.jt.redis.redis_as_db.entity.ExpenseItem;

@Repository
public class ExpenseRepository {

    /**
     *
     */
    private static final String EXPENSE_ITEM = "expenses";

    @Autowired
    private RedisTemplate redisTemplate;

    public ExpenseItem save(ExpenseItem item) {
        redisTemplate.opsForHash().put(EXPENSE_ITEM, item.getId(), item);
        return item;
    }

    public List<Object> findAll() {
        return redisTemplate.opsForHash().values(EXPENSE_ITEM);
    }

    public Object findById(String id) {
        System.out.println("Coming in Repo :" + id);
        return redisTemplate.opsForHash().get(EXPENSE_ITEM, id);
    }

    public void deleteById(String id) {
        redisTemplate.opsForHash().delete(EXPENSE_ITEM, id);
    }
}
