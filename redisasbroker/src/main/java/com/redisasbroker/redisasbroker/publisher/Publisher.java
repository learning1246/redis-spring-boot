package com.redisasbroker.redisasbroker.publisher;

import javax.xml.crypto.dsig.keyinfo.RetrievalMethod;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.listener.ChannelTopic;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.redisasbroker.redisasbroker.dto.Customer;

@RestController
public class Publisher {

    @Autowired
    @Qualifier("dj-redis-template")
    private RedisTemplate template;

    @Autowired
    private ChannelTopic topic;

    @PostMapping("/publish")
    public String publish(@RequestBody Customer customer) {
        template.convertAndSend(topic.getTopic(), "New CUstomer adde");
        return "Event published";
    }
}
