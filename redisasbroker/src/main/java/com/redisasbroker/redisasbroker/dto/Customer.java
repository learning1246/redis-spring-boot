package com.redisasbroker.redisasbroker.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Customer implements Serializable {

    private int id;
    private String name;
    private int loyaltyPoints;
}
