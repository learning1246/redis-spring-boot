package com.redisasbroker.redisasbroker.subscriber;

import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

import com.redisasbroker.redisasbroker.dto.Customer;

@Component
public class CustomerSubscriber implements MessageListener {

    @Override
    public void onMessage(Message message, @Nullable byte[] pattern) {
        // TODO Auto-generated method stub

        System.out.println("Customer received" + message);
    }

}
