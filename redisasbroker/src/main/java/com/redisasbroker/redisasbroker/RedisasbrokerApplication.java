package com.redisasbroker.redisasbroker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RedisasbrokerApplication {

	public static void main(String[] args) {
		SpringApplication.run(RedisasbrokerApplication.class, args);
	}

}
